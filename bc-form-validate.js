(function() {

	angular.module('bcFormValidate')
		.directive('bcFormValidate', function($compile) {

			return {
				restrict: 'A',
				link: function(scope, element, attrs) {
					angular.forEach(element.find('input'), function(node) {
						node = angular.element(node);
						node.removeAttr('field-input');
						node.attr('field-input', '');
						$compile(node)(scope);
					});
				}
			}

		})
		.directive('fieldInput', ['$compile', function($compile) {

			return {
				restrict: 'A',
				scope: {
					validator: '@',
					ngModel: '=',
					invalidMessage: '@'
				},
				link: function(scope, element, attrs) {
					var contentTr = null;
					scope.$watch('ngModel', function(value) {

						var fieldPristine = element.hasClass('ng-pristine');
						var fieldValidator = null;
						var fieldValid = true;

						// Validate Field
						switch (scope.validator) {
							case 'email':
								fieldValidator =
									/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
								fieldValid = fieldValidator.test(value);
								break;
							case 'password':
								var passwordPrefs = {
									upperCaseLetter: /[A-Z]/,
									lowerCaseLetter: /[a-z]/,
									number: /[0-9]/,
									specialCharacter: /[!@#$%^&*]/
								};
								var passwordChecks = {
									length: 8,
									upperCaseLetter: 0,
									lowerCaseLetter: 0,
									number: 0,
									specialCharacter: 0
								};

								if (!value || (value.length < passwordChecks.length)) {
									fieldValid = false;
								} else {
									for (var i = 0; i < value.length; i++) {
										if (passwordPrefs.upperCaseLetter.test(value[i]))
											passwordChecks.upperCaseLetter++;
										else if (passwordPrefs.lowerCaseLetter.test(value[i]))
											passwordChecks.lowerCaseLetter++;
										else if (passwordPrefs.number.test(value[i]))
											passwordChecks.number++;
										else if (passwordPrefs.specialCharacter.test(value[i])) {
											passwordChecks.specialCharacter++;
										}
									}

									if (passwordChecks.upperCaseLetter < 1 ||
										passwordChecks.lowerCaseLetter < 1 ||
										passwordChecks.number < 1 ||
										passwordChecks.specialCharacter < 1) {
										fieldValid = false;
									}
								}

								break;
							case 'required':
								if (!value || (value.length <= 0)) {
									fieldValid = false;
								}
								break;
						}

						var requiredField = element.attr('required');
						if (requiredField && (!value || (value.length <= 0))) {
							fieldValid = false;
						}

						// Add/Remove Field Validation Classes
						if (fieldValid == false && !fieldPristine) {
							element.removeClass('field-invalid');
							if (contentTr) {
								contentTr.remove();
							}
							element.addClass('field-invalid');
							var invalidMessage = scope.invalidMessage ? scope.invalidMessage :
								'Field is required';
							contentTr = angular.element('<span class="error-message">' +
								invalidMessage + '</span>');
							contentTr.insertAfter(element);
							$compile(contentTr)(scope);
						} else {
							var parentElement = angular.element(element[0].parentNode);
							if (parentElement) {
								var errorLabels = parentElement.length > 0 ? parentElement[0].querySelectorAll(
									'.error-message') : parentElement.querySelectorAll(
									'.error-message');
								_.each(errorLabels, function(errorLabel) {
									errorLabel.remove();
								});

								element.removeClass('field-invalid');
								if (contentTr) {
									contentTr.remove();
								}
							}
						}

					});
				}
			}

		}])
		.service('bcFormValidate', function() {

			return {
				validateForm: function(formName) {

					var form = document.getElementById(formName);
					if (form) {
						var invalidFields = form.querySelectorAll('.field-invalid');
						var pristineFields = form.querySelectorAll('.ng-pristine');
						var requiredFields = form.querySelectorAll('[required]');
						var requiredFailure = false;

						_.each(requiredFields, function(rField) {
							_.each(pristineFields, function(pField) {
								if (rField == pField) {
									requiredFailure = true;

									var parentElement = rField.parentNode;
									if (parentElement) {
										var errorLabels = parentElement.length > 0 ? parentElement[0].querySelectorAll(
											'.error-message') : parentElement.querySelectorAll(
											'.error-message');
										_.each(errorLabels, function(errorLabel) {
											errorLabel.remove();
										});

										var element = angular.element(rField);
										element.addClass('field-invalid');
										var contentTr = angular.element('<span class="error-message">' +
											element.attr('invalid-message') + '</span>');
										contentTr.insertAfter(element);
									}
								}
							});
						});

						if (requiredFailure) {
							return false;
						} else {
							return invalidFields.length <= 0;
						}
					}

				}
			}

		});
})(angular.module("bcFormValidate", []));
